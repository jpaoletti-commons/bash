#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset
# set -o xtrace

import log || if [ "$?" -ne 4 ]; then echo "ERR: You need to load the import utility before, i.e source /path/to/import.sh"; fi

function get_remote_version() {
    git fetch
    git show remotes/origin/master:version
}

function init_version() {
    if [ -f "version" ]; then
        log_error "A version already exists .."
        exit 3
    fi

    local version="0.1.0"
    echo $version > version
    log_success "Version initialized with value: $version"
}

function init_version_if_desired() {
    read -p "Do you want to initialize the version ? [Y/n] > " choice
    if [ "$choice" != "n" ]; then
        init_version
    else
        return 4
    fi
}

function get_local_version() {
    if [ ! -f "version" ]; then
        log_error "No $PWD/version file found .. Please create one."
        return 3
    else
        cat version
    fi
}

function assist_update_version() {
    # splits version
    IFS='.' read -ra parts <<< "$1"
    major=${parts[0]}
    minor=${parts[1]}
    patch=${parts[2]}

    # asks for user's choice
    read -p "Which part to increment: (${YELLOW}M${RESET}) Major (${YELLOW}m${RESET}) Minor (${YELLOW}p${RESET}) Patch ? [${MAGENTA}m${RESET}] > " choice
    if [[ "$choice" == "M" ]]; then
        ((major+=1))
        minor=0
        patch=0
    elif [[ "$choice" == "p" ]]; then
        ((patch+=1))
    else
        ((minor+=1))
        patch=0
    fi
    echo "$major.$minor.$patch"
}

