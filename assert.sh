#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset
# set -o xtrace

import log || if [ "$?" -ne 4 ]; then echo "ERR: You need to load the import utility before, i.e source /path/to/import.sh"; fi

# checks if the first argument exists and is not empty. Else displays the second arg as an error message.
# Useful to validate that script's argument are provided.
# Ex. assert_arg "$1" "No arg provided or empty one !"
function assert_arg() {
	arg=${1:-}
	if [ -z "$arg" ]; then
		log_error "$2"
		return 3
	else
		return 0
	fi
}

function assert {
	operand_one=$1
	operator=$2
	operand_two=$3

	case "$operator" in
		is_equal_to)
			if [ "$operand_one" == "$operand_two" ]; then
				return 0;
			else
				log_error "$operand_one is not equal to $operand_two"
				return 3;
			fi
			;;
		is_not_equal_to)
			if [ "$operand_one" != "$operand_two" ]; then
				return 0;
			else
				return 3;
			fi
			;;
		*)
			echo $"Usage: $0 operand {is_equal_to|is_not_equal_to} operand"
			exit 3
	esac
}