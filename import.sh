#!/usr/bin/env bash

# Utility lib to import other bash script library

set -o errexit
set -o pipefail
set -o nounset
# set -o xtrace

# the directory that contains other scripts lib (read only)
# the lib directory is the parent directory of this script
__libs_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
declare -r __libs_dir
# the array holding the scripts lib that are loaded through this import utility script
declare -a __libs
# the integer holding the number of scripts lib that are loaded through this import utility script
declare -i __libs_count=0

##########################################################
# import a given bash library script
# Globals:
#  __libs_dir
#  __libs
#  __libs_count
# Arguments:
#  $1: a bash script file name (mandatory)
# Returns:
#  None
# Exit codes:
# 0: import succeeded
# 3: import failed
# 4: import skipped
##########################################################
function import() {
	# if there is no library arg
	local lib_file="${1:-}"
	if [ -z "$lib_file" ]; then
		echo -e "\\e[31mERR: No library name supplied ..\\e[39m"
		return 3
	fi

	local lib_path="$__libs_dir/$lib_file"

	# appends shell extension if necessary
	if ! [[ "$lib_file" == *\.sh ]]; then
		lib_path+=".sh"
	fi

	# if the lib is not already imported
	if [[ ! " ${__libs[*]+"${__libs[*]}"} " =~ ${lib_path} ]]; then
		# imports the given lib
		#echo "Importing $lib_path .."
		# shellcheck disable=SC1090
		source "$lib_path" || return 3

		# traces the import
		__libs[__libs_count]="$lib_path"
		__libs_count=$((__libs_count + 1))
	fi
}

##########################################################
# displays info about imported libs
# Globals:
#  __libs_dir
#  __libs
#  __libs_count
# Arguments:
#  None
# Returns:
#  None
##########################################################
function libs_info() {
	echo "$__libs_count lib(s)"
	echo "${__libs[*]}"
}

##########################################################
# to check if the import utility is available
# Globals:
#  None
# Arguments:
#  None
# Returns:
#  None
##########################################################
function import_available() {
	return 0
}
