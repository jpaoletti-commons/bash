#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset
# set -o xtrace

import log || if [ "$?" -ne 4 ]; then echo "ERR: You need to load the import utility before, i.e source /path/to/import.sh"; fi
import assert

# get the value of a given property within a given properties file
# usage: getProperty property file
function get_property() {

	property="${1:-}"
	assert_arg "$property" "No property supplied"

	file="${2:-}"
	assert_arg "$file" "No file supplied"

	value=$(cat "$file" | grep "$property=" | cut -d '=' -f 2)

	echo "$value"
}

# gets all properties from a given properties file and stores them as variables replacing . by _ within property key
function get_properties() {

	file="${1:-}"
	assert_arg "$file" "No file supplied"

	if [ -f "$file" ]; then
		while IFS='=' read -r key value || [[ -n "$key" ]]; do
			# skips comment and empty line
			if [[ "$key" != \#* ]] && [ ! -z "$key" ]; then
				key=$(echo $key | tr '.' '_')
				# inject env var if necessary
				if [[ "$value" =~ \$.+ ]]; then
					eval value="${value}" || { log_error "$key=$value but ${YELLOW}$value${RESET} is not set" && return 3; }
				fi
				eval "${key}"=\${value}
			fi
		done <"$file"
	else
		log_error "File '$file' not found"
		exit 1
	fi

}

function get_env_properties() {
	confDir="${1:-}"
	assert_arg "$confDir" "No configuration directory supplied"

	get_properties "$confDir/default.properties"
	get_properties "$confDir/$ENV.properties"

	# loads local configuration if exists
	localConf="$confDir/local.properties"
	if [ -f "$localConf" ]; then
		get_properties "$localConf"
	fi
}
