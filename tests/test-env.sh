#!/bin/bash

set +o errexit
set +o pipefail
set +o nounset
# set -o xtrace

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# import log utilities lib
# shellcheck source=tools/common/log.sh
source "$__dir/log.sh"

# import log utilities lib
# shellcheck source=tools/common/env.sh
source "$__dir/env.sh"

ENV_BACKUP="$ENV"
ENV=
checkEnv || true
echo "Failed"
ENV="test"
checkEnv
ENV="$ENV_BACKUP"

