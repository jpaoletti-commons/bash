#!/bin/bash

set -o pipefail
set -o nounset
# set -o xtrace

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
import_available 2>/dev/null || source "$__dir/../import.sh"
import assert

set +o errexit
assert_arg "" "No shell instructions supplied"
if [[ $? == 1 ]]; then
	echo "** Success **"
else
	echo "** Fail **"
fi

assert_arg "supplied" "No shell instructions supplied"
if [[ $? == 0 ]]; then
	echo "** Success **"
else
	echo "** Fail **"
fi

set -o errexit
