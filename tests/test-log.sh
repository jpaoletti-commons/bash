#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset
# set -o xtrace

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# shellcheck source=scripts/lib/log.sh
source "$__dir/log.sh"

log_header "I am a sample script"
log_success "I am a success message"
log_error "I am an error message"
log_warning "I am a warning message"
log_step "Step one"
log_note "I am a note"
log_square "I am an item with square"
log_square "I am an item with square"
log_square "I am an item with square"
log_done "I am done"
log_bold "I am bold text"
start_time
sleep 3
log_time
