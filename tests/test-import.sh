#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset
# set -o xtrace

# global test result
test_result=false

# the directory of this script 
dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# the temp directory used, within $dir
work_dir=$(mktemp -d -p "$dir")

# deletes the temp directory
function cleanup {  
  rm -rf "$work_dir"
	
	echo
	echo -e "\\e[35mTest Result\\e[39m"
	echo
	if [[ $test_result == true ]]; then
		echo -e "\\e[32m *** SUCCESS ***\\e[39m"
	else
		echo -e "\\e[31m ***   FAIL   ***\\e[39m"
		exit 2
	fi
}

# registers the cleanup function to be called on the EXIT signal
trap cleanup EXIT

echo
echo -e "\\e[35mTesting import lib..\\e[39m"
echo

# shellcheck source=../import.sh
import_available 2>/dev/null || source "$dir/../import.sh" && echo -e "\\e[32mSourcing import script: ok\\e[39m"

# checks that calling import on the log lib does not raise any error
import log && echo -e "\\e[32mCall import: ok\\e[39m"

# checks that importing twice the same lib leads to skipping the second import
import log && exit 2 || echo -e "\\e[32mSkip feature: ok\\e[39m"

# checks error on importing wrong file
import abcdefg && exit 2 || echo -e "\\e[32mFail on wrong file: ok\\e[39m"

# checks transitive import
file_one="testImportToDeleteOne.sh"
file_two="testImportToDeleteTwo.sh"
dir_files=$(basename "$work_dir")
echo -e "import log\\nfunction testImport() { echo 'ok'; }" >"$work_dir/$file_one"
echo "import tests/$dir_files/$file_one" >"$work_dir/$file_two"
{ import "tests/$dir_files/$file_two" && echo -e "\\e[32mTransitive import: ok\\e[39m"; } || { echo -e "\\e[31mTransitive import failed ..\\e[39m" && exit 2; }
result=$(testImport) || true
if [[ $result == "ok" ]]; then
	echo -e "\\e[32mDependency call: ok\\e[39m"
else
	echo -e "\\e[31mDependency call failed ..\\e[39m"
	exit 2
fi

# if it goes there then everything is ok
test_result=true