#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset
# set -o xtrace

#this_script_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

import log || if [ "$?" -ne 4 ]; then echo "ERR: You need to load the import utility before, i.e source /path/to/import.sh"; fi

function check_service_is_listening() {
	
	host=${1:-127.0.0.1}
	port=${2:-8080}

	success=true
	{ echo >/dev/tcp/"$host/$port"; } &> /dev/null || success=false

	if [ $success == false ]; then
		log_fail "No service listening on ${YELLOW}$host:$port${RESET} .."
		return 3
	else
		log_success "Service listening on ${YELLOW}$host:$port${RESET}"
	fi
}

function check_service_health_is_ok() {
	host=${1:-127.0.0.1}
	port=${2:-8080}

	http_code=$(curl -XGET -I -s -L "http://$host:$port/health" | grep 'HTTP/' | cut -d ' ' -f 2)

	if [ "$http_code" != 200 ]; then
		log_fail "Service's health is not ok at ${YELLOW}$host:$port/health${RESET} .."
		return 3
	else
		log_success "Service's health is ok at ${YELLOW}$host:$port/health${RESET}"
	fi
}

function check_env_is_set() {
		local environment=${ENV:-}
    if [ -z "$environment" ]; then
        log_fail "The environment variable 'ENV' does not exist"
        return 3
    fi
    log_success "Environment: $ENV"
}

function check_docker() {
	docker_version=$(docker --version)
	if [ -z "$docker_version" ]; then
		log_fail "Docker is not installed"
		return 3
	fi
	log_success "$docker_version"
	{ docker ps &>/dev/null && log_success "Docker is running"; } || { log_fail "Docker is not available. Is docker running ?" && return 3; }
}