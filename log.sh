#!/usr/bin/env bash

# colors
BLACK=$(tput setaf 0)
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
BLUE=$(tput setaf 4)
MAGENTA=$(tput setaf 5)
CYAN=$(tput setaf 6)
WHITE=$(tput setaf 7)
GREY=$(tput setaf 238)


# background
#BLUE_BG=$(tput setab 4)

# style
BOLD=$(tput bold)
RESET=$(tput sgr0)
#REVERSE=$(tput smso)
#UNDERLINE=$(tput smul)

#!/bin/bash
color(){
    for c; do
        printf '\e[48;5;%dm%03d' $c $c
    done
    printf '\e[0m \n'
}

print_colors_table() {
	IFS=$' \t\n'
	color {0..15}
	for ((i=0;i<6;i++)); do
			color $(seq $((i*36+16)) $((i*36+51)))
	done
	color {232..255}
}


# Headers and Logging
log_header() {
	printf "\\n🔹🔹🔹  ${BOLD}%s${RESET} 🔹🔹🔹\\n\\n" "$@"
}
log_step() {
	printf " ➜  %s\\n" "$@"
}
log_done() {
	printf "🏁  %s\\n" "$@"
}
log_success() {
	printf "✔️  ${GREEN}%s${RESET}\\n" "$@"
}
log_fail() {
	printf "❌  ${RED}%s${RESET}\\n" "$@"
}
log_error() {
	>&2 printf "❗❗ ${RED}%s${RESET}\\n" "$@"
}
log_warning() {
	printf "⚠️  ${YELLOW}%s${RESET}\\n" "$@"
}
#log_underline() { printf "${UNDERLINE}%s${RESET}\\n" "$@"
#}
log_bold() {
	printf "${BOLD}%s${RESET}\\n" "$@"
}
log_note() {
	printf "📌  ${BOLD}%s${RESET}\\n" "$@"
}
log_square() {
	printf " ▫️  %s\\n" "$@"
}
log_square_key_value() {
	printf " ▫️  %s: ${CYAN}%s${RESET}\\n" "$@"
}
start_time() {
	SECONDS=0
}
log_time() {
	printf "⏱️  ${YELLOW}%s${RESET}h ${YELLOW}%s${RESET}m ${YELLOW}%s${RESET}s\\n" $((SECONDS / 3600)) $(((SECONDS / 60) % 60)) $((SECONDS % 60))
}

log_db_properties() {

	obfuscated_password=$(echo "$password" | sed -e 's/./*/g')

    log_square_key_value "Host     " "$host"
    log_square_key_value "Port     " "$port"
    log_square_key_value "User     " "$user"
    log_square_key_value "Password " "$obfuscated_password"
    log_square_key_value "Database " "$database"
}

log_hr() {
	local COLS
	COLS="$(tput cols)"
	if ((COLS <= 0)); then
		COLS="${COLUMNS:-80}"
	fi

	head -c $COLS < /dev/zero | tr '\0' '_'
}

log_box() {
	local s=("$@") b="" w=""
	for l in "${s[@]}"; do
		((w < ${#l})) && {
			b="$l"
			w="${#l}"
		}
	done
	printf "${YELLOW}"
	echo "┏━${b//?/━}━┓
┃ ${b//?/ } ┃"
	for l in "${s[@]}"; do
		printf '┃ %s%*s%s ┃\n' "${BLUE}" "-$w" "$l" "${YELLOW}"
	done
	echo "┃ ${b//?/ } ┃
┗━${b//?/━}━┛"
	printf "${RESET}"
}
