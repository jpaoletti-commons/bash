# Bash commons<!-- omit in toc -->

A collection of useful bash scripts

- [Installation](#installation)
- [Usage](#usage)
    - [Import](#import)
    - [Argument](#argument)

## Installation

bash commons can be installed in your **git** project as a **git module**, with the following command:

```bash
git submodule add https://gitlab.com/jpaoletti-commons/bash.git
```

## Usage

The first thing is to setup the import utility, to be able to import the other bash utilities.

### Import

You can **import** bash commmons scripts by using the `import` function provided by the import.sh script, like this:

```bash
# Loads the import bash utility if necessary
import_available 2>/dev/null || source "bash/import.sh"

# then imports a bash utility, for instance the log.sh (.sh extension is optional)
import log
```

### Argument

To check if an arg is supplied :
```bash
import assert
assert_arg $1 "No argument supplied"
```

### Log
TODO

### Properties
TODO

### Environment
TODO

## Sub module

To update the sub module:
```bash
git pull origin master
```

or, once only:
```bash
git stash
git checkout master
git stash pop
```

and then 
```bash
git pull
```
(every time you need to update)

